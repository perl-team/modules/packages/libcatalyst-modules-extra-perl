#!/bin/sh

set -e

PERL5LIB="$DESTDIR/usr/share/perl5/"

export PERL5LIB

echo "=========================================================================="
echo "Make $1"
echo "PERL5LIB = $PERL5LIB"
echo "DESTDIR = $DESTDIR"
echo "=========================================================================="

#--- unpacking tar.gz in proper folders
if [ "$1" = "unpack" ]; then
	test -d build-area || mkdir build-area
	
	cd tarballs
	for dir in *
	do
		if [ -d $dir ]; then
			cd $dir
			for file in *.tar.gz
			do
				test -d ../../build-area/$dir || mkdir ../../build-area/$dir
				tar -C ../../build-area/$dir -xzf $file
			done
			cd ..
		fi
	done
	cd ..
else
	cd build-area
	for dir in *
	do
		if [ -d $dir ]; then
			cd $dir
			for module in *
			do
				cd ../..
				set -e
				d=build-area/$dir/$module
				dh_auto_configure -D $d
				dh_auto_build -D $d
				dh_auto_test -D $d
				dh_auto_install -D $d
				set +e
				cd build-area/$dir
			done
			cd ..
		fi
	done
	cd ..
fi
